/*
 * @Author: your name
 * @Date: 2020-01-08 09:57:34
 * @LastEditTime : 2020-01-08 10:25:36
 * @LastEditors  : Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \vhr_dev\src\utils\sunny.js
 */
// 按钮防抖  
function debounce(func, wait=1000){ 
  let timeout;
  return function(event){
   clearTimeout(timeout)
   timeout = setTimeout(()=>{
    func.call(this, event)
   },wait)
  }
 }
 export {debounce}