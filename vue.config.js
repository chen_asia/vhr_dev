/*
 * @Author: your name
 * @Date: 2019-12-29 11:00:50
 * @LastEditTime : 2020-01-05 11:29:58
 * @LastEditors  : Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \vuehr\vue.config.js
 */
let test = process.env.NODE_ENV
// development 192.168.1.8
console.log(test)

let proxyObj = {};
proxyObj['/ws'] = {
    ws: true,
    target: "http://devl.xmwzx.org.cn:8081"
    // target: "http://192.168.1.8:8081"
};
console.log(proxyObj)
proxyObj['/'] = {
    ws: false,
    target: 'http://devl.xmwzx.org.cn:8081',
    // target: "http://192.168.1.8:8081",
    changeOrigin: true,
    pathRewrite: {
        '^/': ''
    }
}
module.exports = {
    devServer: {
        host: 'localhost',
        port: 8080,
        proxy: proxyObj
    }
}