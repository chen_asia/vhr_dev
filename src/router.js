/*
 * @Author: your name
 * @Date: 2020-01-01 20:54:53
 * @LastEditTime : 2020-01-09 14:28:57
 * @LastEditors  : Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \vhr_dev\src\router.js
 */
import Vue from 'vue'
import Router from 'vue-router'
import Login from './views/Login.vue'
import Home from './views/Home.vue'
import FriendChat from './views/chat/FriendChat.vue'

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            name: 'Login',
            component: Login,
            hidden:true
        }, {
            path: '/home',
            name: 'Home',
            component: Home,
            hidden:true,
            meta:{
                roles:['admin','user']
            },
            children:[
                {
                    path: '/chat',
                    name: '在线聊天',
                    component: FriendChat,
                    hidden:true
                },
                {
                    path: '/detailEdit',
                    name: '应收单编辑页面',
                    component:() =>  import(/* webpackChunkName: "detailEdit" */ './views/ordermanageDjysd/detailEdit.vue'),
                    hidden:true
                },
                {
                    path: '/djysdDetailView',
                    name: '应收单查看页面',
                    component:() =>  import(/* webpackChunkName: "detailEdit" */ './views/ordermanageDjysd/djysdDetailView.vue'),
                    hidden:true
                },
                {
                    path: '/djsyqDetailEdit',
                    name: '使用单编辑页面',
                    component:() =>  import(/* webpackChunkName: "detailEdit" */ './views/ordermanageDjsyd/djsyqDetailEdit.vue'),
                    hidden:true
                },
                {
                    path: '/djsyqDetailView',
                    name: '使用单查看页面',
                    component:() =>  import(/* webpackChunkName: "detailEdit" */ './views/ordermanageDjsyd/djsyqDetailView.vue'),
                    hidden:true
                },
                {
                    path: '/djsqdDetailEdit',
                    name: '收取单编辑页面',
                    component:() =>  import(/* webpackChunkName: "detailEdit" */ './views/ordermanageDjsqd/djsqdDetailEdit.vue'),
                    hidden:true
                },
                {
                    path: '/djsqdDetailView',
                    name: '收取单查看页面',
                    component:() =>  import(/* webpackChunkName: "detailEdit" */ './views/ordermanageDjsqd/djsqdDetailView.vue'),
                    hidden:true
                }
            ]
        }
    ]
})