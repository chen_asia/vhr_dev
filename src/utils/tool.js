/*
 * @Author: your name
 * @Date: 2020-01-04 20:07:36
 * @LastEditTime : 2020-01-08 10:20:14
 * @LastEditors  : Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \vhr_dev\src\utils\tool.js
 */
export default {
  //  参数转化 为 &链接的 字符串
  pars(param) {
    var result = ""
    for (let name in param) {
        if (typeof param[name] != 'function') {
            result += "&" + name + "=" + encodeURI(param[name]);
        }
    }
    return result.substring(1)
  },
  debounce(func, wait=1000){ 
    let timeout;
    return function(event){
     clearTimeout(timeout)
     timeout = setTimeout(()=>{
      func.call(this, event)
     },wait)
    }
   }
}