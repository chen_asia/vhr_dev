/*
 * @Author: your name
 * @Date: 2020-01-05 13:17:59
 * @LastEditTime : 2020-01-05 13:28:01
 * @LastEditors  : Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \vhr_dev\src\utils\filters.js
 */
let NumberSet = value => {
     // 截取当前数据到小数点后2位
  const tempVal = Number(value).toFixed(3)
  if (tempVal === 'NaN') {
    return '0.00'
  }
  const realVal = tempVal.substring(0, tempVal.length - 1)
  return realVal
  }
  export { NumberSet }